**Product Name**

Obejct Identifier

**Product Link**

[Link](https://www.researchgate.net/publication/337464355_OBJECT_DETECTION_AND_IDENTIFICATION_A_Project_Report)

**Product Short Description**

Feed a sample image to the algorithm and expect the algorithm to detect and identify objects in the image and label them according to the class assigned to it.

**Product is combination of features**

1. Object detection
2. Person Detection

**Product is provided by which company?**

Personal Project

-------------------------------------------------------------------------------------------------------------------------------------------------------

**Product Name**

Crowd Counting with Decomposed Uncertainty

**Product Link**

[Link](https://www.research.ibm.com/artificial-intelligence/publications/paper/?id=Crowd-Counting-with-Decomposed-Uncertainty)

**Product Short Description**

It focuses on uncertainty estimation in the domain of crowd counting. With increasing occurrences of heavily crowded events such as political rallies, protests, concerts, etc., automated crowd analysis is becoming an increasingly crucial task. A neural network framework that can be scaled with quantification of decomposed uncertainty using a bootstrap ensemble has been provided.

**Product is combination of features**

1. Person Detection

**Product is provided by which company?**

IBM
