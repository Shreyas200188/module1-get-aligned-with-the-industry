**What is CNN?**

*Convolutional Neural Networks* is a type of neural network used for mainly processiong data which is in grid format, for ex. image processing. It is typically made up of three layers:
1. Convolutional layers
2. Pooling layers
3. Fully Connected layers

*Convolutional Layer* : In this layer,a small grid of parameters is chosen from the actual desired output class and it is convolved through the grid of parameters in the input. This is called a **kernel** and it is optimizable therefore it helps recognize new features in an image no matter where in the picture it is located. 

This layer is basically a feature extraction layer i.e. helps the network find out the features that exist in the output and their locations. The input grid map is called a *input tensor* and the summation of the element wise product of the input tensor and the kernel is used to obtain the output value in the corresponding loaction of the output tensor, called the **feautre map**.

Many kernels produce many such feature maps.

In this method the centre of the kernel never reaches the outermost boundary parameter of the input tensor and thus we add something to it called - **Padding**. A zero single layer padding is added to all sides of the input tensor so that the output feature map is of the same size as that of the input tensor.

The distance between two successive kernel positions is called a **stride**, which also defines the convolution operation.The number of kernels, size of kernels, stride and padding of the kernel are hyperparameters which can be altered. The outputs of the convolution are then passed through a nonlinear activation function, such as sigmoid or tanh function. Although nowadays ReLU functions is proving to be a must fater learner.

*Pooling Layer* : A pooling layer performs the operation which reduces the in-plane dimensionality of the feature maps in order to reduce the variance which arises due to small shifts and disorientations, and decrease the number of extra learnable parameters. It makes the feature maps one dimensional i.e. *flattened*.

The flattened feature maps forms a one dimensional array/grid of parameters which then act as the first layer to a DNN and the first layer + all the hidden layers + the output layer form the *fully connected layers*.
+